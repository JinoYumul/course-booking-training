// Solution Source: https://remysharp.com/2019/11/04/nice-imports-with-nextjs

const path = require('path')
const webpack = require('webpack')

module.exports = {
    webpack: config => {
        // Setting the configuration to allow import using absolute link.
        config.resolve.alias['~'] = path.resolve(__dirname)
        return config
    }
}
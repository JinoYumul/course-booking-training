//array to contain possible errors encountered in our booking app
//fields are to be used as props when passed into the Banner component
const errors = [
    {
        id: '1',
        title: 'Internal server error',
        content: 'Something unexpected happened. Please try again later.',
        destination: '/courses',
        label: 'Back to courses'
    },
    {
        id: '2',
        title: 'Duplicate email',
        content: 'The email you submitted is already registered.',
        destination: '/login',
        label: 'Login'
    },
    {
        id: '3',
        title: 'Failed authentication',
        content: 'Registered already? If so, kindly check email and password.',
        destination: '/login',
        label: 'Login'
    },
]

//function to get all possible id's to be used in dynamic routing
export function getAllErrorIds() {
    //the data structure to be returned has to follow the exact same format as follows:
    return errors.map(error => {
        return {
            params: {
                id: error.id
            }
        }
    })
}

//function to return a particular error from the array of possible errors
export function getErrorData(id) {
    const errorData = errors.find(error => error.id == id)
    return errorData
}
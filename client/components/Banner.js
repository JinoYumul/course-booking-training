import Link from 'next/link'
import PropTypes from 'prop-types'
import { Jumbotron, Row, Col } from 'react-bootstrap'

const Banner = ({ data }) => {
    const { title, content, destination, label } = data

    return (
        <Jumbotron>
            <h1>{ title }</h1>
            <p>{ content }</p>
            <Link href={ destination }><a className="btn btn-primary">{ label }</a></Link>
        </Jumbotron>
    )
}

Banner.propTypes = {
    data: PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        destination: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
    })
}

export default Banner
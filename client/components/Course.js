import { useState, useEffect, useContext } from 'react'
import { Card, Button } from 'react-bootstrap'
import PropTypes from 'prop-types'
import Swal from 'sweetalert2'

import UserContext from '~/contexts/UserContext'
import AppHelper from '~/app-helper'

const Course = ({ course }) => {
    const { user } = useContext(UserContext)
    const { id, name, description, price } = course

    const enroll = () => {
        const options = {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json', 
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }` 
            },
            body: JSON.stringify({ courseId: course._id })
        }

        fetch(`${ AppHelper.API_URL }/users/enroll`, options).then(AppHelper.toJSON).then(isEnrolled => {
            if (isEnrolled === true) {
                Swal.fire('Enrollment Successful', 'Thank you for enrolling.', 'success')
            } else {
                Router.push('/errors/1')
            }
        })
    }

    return (
        <Card className="mb-3">
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <p>{ description }</p>
                <p>Price: ₱{ price }</p>
                { (user.id !== null) ? <Button variant="primary" onClick={ enroll }>Enroll</Button> : null }
            </Card.Body>
        </Card>
    )
}

Course.propTypes = {
    course: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}

export default Course
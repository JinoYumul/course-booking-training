import { useContext, useState, Fragment } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'

import UserContext from '~/contexts/UserContext'

export default () => {
    const { user } = useContext(UserContext)
    const [isExpanded, setIsExpanded] = useState(false)

    let NavOptions

    if (user.id !== null) {
        if (user.isAdmin === true) {
            NavOptions = (
                <Fragment>
                    <Link href="/courses/create"><a className="nav-link">Add Course</a></Link>
                    <Link href="/logout"><a className="nav-link">Logout</a></Link>
                </Fragment>
            )
        } else {
            NavOptions = (
                <Fragment>
                    <Link href="/logout"><a className="nav-link">Logout</a></Link>
                </Fragment>
            )
        }
    } else {
        NavOptions = (
            <Fragment>
                <Link href="/login"><a className="nav-link">Login</a></Link>
                <Link href="/register"><a className="nav-link">Register</a></Link>
            </Fragment>
        )
    }

    return (
        <Navbar expanded={ isExpanded } bg="dark" expand="lg" fixed="top" variant="dark">
            <Link href="/"><a className="navbar-brand">React-Booking</a></Link>
            <Navbar.Toggle onClick={ () => setIsExpanded(!isExpanded) } aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto" onClick={ () => setIsExpanded(!isExpanded) }>
                    <Link href="/courses"><a className="nav-link" role="button">Courses</a></Link>
                    { NavOptions }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

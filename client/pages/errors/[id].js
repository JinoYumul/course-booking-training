import Banner from '~/components/Banner'
import { getAllErrorIds, getErrorData } from '~/data/errors'

export default ({ errorData }) => { 
    return <Banner data={ errorData }/>
}

// Retrieve all possible IDs for dynamic endpoints.
export async function getStaticPaths() {
    const paths = getAllErrorIds()
    
    return {
        paths,
        fallback: false
    }
}

// Statically generate error data using the given ID from the URL parameter.
export async function getStaticProps({ params }) {
    const errorData = getErrorData(params.id)

    return {
        props: {
            errorData
        }
    }
}
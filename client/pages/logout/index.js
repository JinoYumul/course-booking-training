import { useContext, useEffect, Fragment } from 'react'
import Router from 'next/router'

import UserContext from '~/contexts/UserContext'

export default () => {
    const { unsetUser } = useContext(UserContext)
    
    useEffect(() => {
        unsetUser()
        Router.push('/login')
    }, [])

    return (
        <Fragment>
            <h5 className="text-center">Logging out...</h5>
            <h6 className="text-center">You will be redirected back to the login page shortly.</h6>
        </Fragment>
    )
}
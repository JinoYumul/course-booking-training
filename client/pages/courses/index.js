import { useState, useContext, useEffect, Fragment } from 'react'
import { Table, Button, Modal, Form } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '~/contexts/UserContext'
import AppHelper from '~/app-helper'
import View from '~/components/View'
import Course from '~/components/Course'

export async function getServerSideProps() {
    const res = await fetch('http://localhost:4000/api/courses')
    const data = await res.json()

    return { props: { data } }
}

export default ({ data }) => {
    const { user } = useContext(UserContext)

    const [course, setCourse] = useState({})
    const [isModalEditCourseShown, setIsModalEditCourseShown] = useState(false)

    const openModalEditCourse = (courseId) => {
        fetch(`${ AppHelper.API_URL }/courses/${ courseId }`).then(AppHelper.toJSON).then(data => {
            setCourse({
                courseId: courseId,
                name: data.name,
                description: data.description,
                price: data.price
            })
            setIsModalEditCourseShown(true)
        })
    }

    const disableCourse = (courseId) => {
        const payload = {
            method: 'DELETE',
            headers: { 'Authorization': `Bearer ${ AppHelper.getAccessToken() }` }
        }

        fetch(`${ AppHelper.API_URL }/courses/${ courseId }`, payload).then(AppHelper.toJSON).then(isDisabled => {
            if (isDisabled === true) {
                Swal.fire('Course Disabled', 'The course has been successfully disabled.', 'success')
                Router.push('/courses')
            } else {
                Router.push('/errors/1')
            }
        })
    }

    const CourseCards = () => {
        return data.map(course => <Course key={ course._id } course={ course }/>)
    }

    const AdminTable = () => {
        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    { 
                        data.map(course => {
                            return (
                                <tr key={ course._id }>
                                    <td>{ course.name }</td>
                                    <td>₱ { course.price }</td>
                                    <td>{ course.isActive ? 'Open': 'Closed' }</td>
                                    <td width="1%" className="text-nowrap">
                                        <Button onClick={ () => openModalEditCourse(course._id) }>Edit</Button>&nbsp;
                                        <Button variant="danger" onClick={ () => disableCourse(course._id) }>Disable</Button>
                                    </td>
                                </tr>
                            )
                        }) 
                    }
                </tbody>
            </Table>
        )
    }

    const CourseView = () => {
        if (user.isAdmin === true) {
            return (
                <Fragment>
                    <AdminTable/>
                    <ModalEditCourse 
                        isModalEditCourseShown={ isModalEditCourseShown } 
                        setIsModalEditCourseShown={ setIsModalEditCourseShown } 
                        course={ course }/>
                </Fragment>
            )
        } else {
            return (
                <CourseCards/>
            )
        }
    }
    
    return (
        <View title="Courses">
            <h3>Courses</h3> 
            <CourseView/>
        </View>
    )
}

const ModalEditCourse = ({ isModalEditCourseShown, setIsModalEditCourseShown, course }) => {
    const [name, setName] = useState(course.name)
    const [description, setDescription] = useState(course.description)
    const [price, setPrice] = useState(course.price)

    useEffect(() => {
        setName(course.name)
        setDescription(course.description)
        setPrice(course.price)
    }, [course])

    const editCourse = (e) => {
        e.preventDefault()

        const payload = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({
                courseId: course.courseId,
                name: name,
                description: description,
                price: price
            })
        }

        fetch(`${ AppHelper.API_URL }/courses`, payload).then(AppHelper.toJSON).then(isSuccessful => {
            if (isSuccessful === true) {
                Swal.fire('Update Successful', 'The course details have been successfully updated.', 'success')
                setIsModalEditCourseShown(false)
            } else {
                setIsModalEditCourseShown(false)
                Router.push('/errors/1')
            }
        })
    }
    
    return (
        <Modal show={ isModalEditCourseShown } onHide={ () => setIsModalEditCourseShown(false) }>
            <Modal.Header closeButton>
                <Modal.Title>Edit Course</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={ (e) => editCourse(e) }>
                    <Form.Group controlId="courseName">
                        <Form.Label>Course Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter course name" value={ name } onChange={ (e) => setName(e.target.value) } required/>
                    </Form.Group>
                    <Form.Group controlId="description">
                        <Form.Label>Course Description:</Form.Label>
                        <Form.Control as="textarea" rows="3" placeholder="Enter course description" value={ description } onChange={ (e) => setDescription(e.target.value) } required/>
                    </Form.Group>
                    <Form.Group controlId="price">
                        <Form.Label>Course Price:</Form.Label>
                        <Form.Control type="number" value={ price } onChange={ (e) => setPrice(e.target.value) } required/>
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit</Button>&nbsp;
                    <Button variant="warning" onClick={ () => setIsModalEditCourseShown(false) }>Cancel</Button>
                </Form>
            </Modal.Body>
        </Modal>
    )
}
import { useState } from 'react'
import { Form, Button, Row, Col, Card } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

import AppHelper from '~/app-helper'
import View from '~/components/View'

export default () => {
    return (
        <View title="New Course">
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>New Course</h3>
                    <Card>
                        <Card.Header>Course Information</Card.Header>
                        <Card.Body>
                            <NewCourseForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

const NewCourseForm = () => {
    const [courseName, setCourseName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(0)

    const createCourse = (e) => {
        e.preventDefault()

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({
                name: courseName,
                description: description,
                price: price
            })
        }
        
        fetch(`${ AppHelper.API_URL }/courses`, payload).then(AppHelper.toJSON).then(isSuccessful => {
            if (isSuccessful === true) {
                Swal.fire('Course Added', 'The new course has been successfully created.', 'success')
                Router.push('/courses')
            } else {
                Router.push('/errors/1')
            }
        })
    }

    return (
        <Form onSubmit={ (e) => createCourse(e) }>
            <Form.Group controlId="courseName">
                <Form.Label>Course Name:</Form.Label>
                <Form.Control type="text" placeholder="Enter course name" value={ courseName } onChange={ (e) => setCourseName(e.target.value) } required/>
            </Form.Group>
            <Form.Group controlId="description">
                <Form.Label>Course Description:</Form.Label>
                <Form.Control as="textarea" rows="3" placeholder="Enter course description" value={ description } onChange={ (e) => setDescription(e.target.value) } required/>
            </Form.Group>
            <Form.Group controlId="price">
                <Form.Label>Course Price:</Form.Label>
                <Form.Control type="number" value={ price } onChange={ (e) => setPrice(e.target.value) } required/>
            </Form.Group>
            <Button variant="primary" type="submit">Submit</Button>
        </Form>
    )
}
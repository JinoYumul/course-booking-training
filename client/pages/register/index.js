import { useState, useEffect, Fragment } from 'react'
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import Router from 'next/router'
import Swal from 'sweetalert2'

import AppHelper from '~/app-helper'
import View from '~/components/View'

/**
 * Registration page for new users.
 */
export default () => {
    return (
        <View title={ 'Register' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Register an Account</h3>
                    <Card>
                        <Card.Header>User Information</Card.Header>
                        <Card.Body>
                            <RegisterForm/>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </View>
    )
}

/**
 * Registration form for completion of registrants.
 */
const RegisterForm = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')
    const [isSubmitEnabled, setIsSubmitEnabled] = useState(false)

    // Effect hook for checking validity of password, confirmPassword, and mobileNumber inputs.
    useEffect(() => {
        const doPasswordsHaveValue = (password !== '' && confirmPassword !== '')
        const doPasswordsMatch = (confirmPassword === password)
        const isMobileNumberValid = (mobileNumber.length === 11)

        setIsSubmitEnabled((doPasswordsHaveValue && doPasswordsMatch && isMobileNumberValid))
    }, [password, confirmPassword, mobileNumber])

    /**
     * Pre-registration check for possible email duplicates.
     * @param {Object} e 'The form event object' 
     */
    const checkForEmailDuplicates = (e) => {
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ email: email })
        }

        fetch(`${ AppHelper.API_URL }/users/email-exists`, options).then(AppHelper.toJSON).then(hasDuplicates => {
            (!hasDuplicates) ? register() : Router.push('/errors/2')
        })
    }

    /**
     * Actual registration request to API endpoint.
     */
    const register = () => {
        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password,
                mobileNumber: mobileNumber
            })
        }

        fetch(`${ AppHelper.API_URL }/users`, options).then(AppHelper.toJSON).then(isRegistered => {
            (isRegistered) ? Router.push('/login') : Router.push('/errors/1')
        })
    }

    return (
        <Form onSubmit={ checkForEmailDuplicates }>
            <Row>
                <Col xs="12" sm="6">
                    <Form.Group controlId="firstName">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" value={ firstName } onChange={ (e) => setFirstName(e.target.value) } autoComplete="off" required/>
                    </Form.Group>
                </Col>
                <Col xs="12" sm="6">
                    <Form.Group controlId="lastName">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" value={ lastName } onChange={ (e) => setLastName(e.target.value) } autoComplete="off" required/>
                    </Form.Group>
                </Col>
            </Row>
            <Form.Group controlId="mobileNumber">
                <Form.Label>Mobile No.</Form.Label>
                <Form.Control type="number" value={ mobileNumber } onChange={ e => setMobileNumber(e.target.value) } autoComplete="off" required/>
            </Form.Group>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" value={ email } onChange={ e => setEmail(e.target.value) } autoComplete="off" required/>
                <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
            </Form.Group>
            <Row>
                <Col xs="12" sm="6">
                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={ password } onChange={ e => setPassword(e.target.value) } required/>
                    </Form.Group>
                </Col>
                <Col xs="12" sm="6">
                    <Form.Group controlId="confirmPassword">
                        <Form.Label>Verify Password</Form.Label>
                        <Form.Control type="password" value={ confirmPassword } onChange={ e => setConfirmPassword(e.target.value) } required/>
                    </Form.Group>
                </Col>
            </Row>
            <Button variant="success" type="submit" block disabled={ !isSubmitEnabled }>Submit</Button>
        </Form>
    )
}
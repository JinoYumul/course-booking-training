const express = require("express")
const router = express.Router()
const UserController = require("../controllers/user")
const auth = require("../auth")

//User registration
router.post("/", (req, res) => { //req = request, res = response
	UserController.register(req.body).then(resultFromRegister => res.send(resultFromRegister))
})

//Check if email exists
router.post("/email-exists", (req, res) => {
	UserController.emailExists(req.body).then(resultFromEmailExists => res.send(resultFromEmailExists))
})

//User login
router.post('/login', (req, res) => {
	UserController.login(req.body).then(resultFromLogin => res.send(resultFromLogin))
})

//Get user details
router.get('/details', auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)

	UserController.get({userId: user.id}).then(user => res.send(user))
})

//Enroll for a course
router.put('/enroll', auth.verify, (req, res) => {
	let params = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	UserController.enroll(params).then(resultFromEnroll => res.send(resultFromEnroll))
})

module.exports = router //makes the file exportable